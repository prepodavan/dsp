import os
import json
import models
import handlers
import settings
import telegram
from telegram.ext import Updater

bot_cfg = settings.config['bot']
updater = Updater(token=bot_cfg['TOKEN'],use_context=True)
session = models.create_all(settings.config['postgres'])
handlers.init_handlers(updater.dispatcher, session)

updater.start_webhook(
	listen=bot_cfg['HOST'],
	port=bot_cfg['PORT'],
	url_path=bot_cfg['TOKEN'])

updater.bot.set_webhook(bot_cfg['WEBHOOK'])

