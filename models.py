from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, LargeBinary, DateTime, create_engine
from sqlalchemy.orm import sessionmaker


Base = declarative_base()

def create_all(config):
	engine = create_engine(config['URL'], echo=False)
	session = sessionmaker(bind=engine)()
	Base.metadata.create_all(engine)
	return session



class Photo(Base):
	__tablename__ = 'photoes'

	id = Column(Integer, primary_key=True)
	uid = Column(Integer)
	datetime = Column(DateTime)
	body = Column(LargeBinary)

	def __repr__(self):
		return f'<{self.__class__.__name__}(id={self.id}, uid={self.uid}, datetime={self.datetime})>'


class Audio(Base):
	__tablename__ = 'audios'

	id = Column(Integer, primary_key=True)
	uid = Column(Integer)
	datetime = Column(DateTime)
	source = Column(LargeBinary)
	wav = Column(LargeBinary)

	def __repr__(self):
		return f'<{self.__class__.__name__}(id={self.id}, uid={self.uid}, datetime={self.datetime})>'

