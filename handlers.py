import datetime
import subprocess
from pathlib import Path
from settings import config
from models import Audio, Photo
from recognition import count_faces
from telegram.ext import MessageHandler, Filters, CommandHandler


def start(update, context):
	update.message.reply_text(text=config['messages']['greetings'])

def src(update, context):
	update.message.reply_text(text=config['messages']['repo'])

def letter(update, context):
	update.message.reply_text(text=config['messages']['letter'])

def convert_audio(update, context):
	tmp_source_filepath = Path(f'./{update.message.message_id}.mp3').resolve()
	tmp_converted_filepath = Path(f'./{update.message.message_id}.wav').resolve()
	update.message.audio.get_file().download(custom_path=str(tmp_source_filepath))

	cmd = [
		'ffmpeg', 
		'-i', 
		str(tmp_source_filepath),
		'-ar',
		'16000',
		str(tmp_converted_filepath),
	]
	subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	return tmp_source_filepath, tmp_converted_filepath

def audio(session):
	def wrapped_audio(update, context):
		try:
			context.bot.send_message(
				chat_id=update.effective_chat.id, 
				text='Just wait a little while we are processing ur audio')

			source, wav = convert_audio(update, context)
			update.message.reply_audio(
				audio=wav.open('rb'),
				title=f'{update.message.audio.title}.wav')

			converted_audio = Audio(
				uid=update.effective_user.id,
				datetime=datetime.datetime.now(),
				source=source_path.open('rb').read(),
				wav=wav_path.open('rb').read(),
			)
			session.add(converted_audio)
			session.commit()
		except Exception as e:
			context.bot.send_message(
				chat_id=update.effective_chat.id, 
				text='Error occured while saving ur audio')
		finally:
			source.unlink()
			wav.unlink()

	return wrapped_audio

def save_image_and_notify_user(update, context, session, image):
	try:
		photo = Photo(
			uid=update.effective_user.id,
			datetime=datetime.datetime.now(),
			body=image,
		)
		session.add(photo)
		session.commit()

		context.bot.send_message(
			chat_id=update.effective_chat.id, 
			text='Saved',
		)
	except Exception as e:
		context.bot.send_message(
			chat_id=update.effective_chat.id, 
			text='Error occured while saving ur image',
		)

def photo(session):
	def wrapped_photo(update, context):
		context.bot.send_message(
			chat_id=update.effective_chat.id, 
			text='Just wait a little while we are processing ur image')
		largest_image = update.message.photo[-1]
		image = largest_image.get_file().download_as_bytearray()
		amount_of_faces = count_faces(image)
		msg = f"{amount_of_faces} found on this image.{' Saving' if amount_of_faces > 0 else ''}"
		context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
		if amount_of_faces > 0:
			save_image_and_notify_user(update, context, session, image)

	return wrapped_photo

def init_handlers(dispatcher, db_session):
	dispatcher.add_handler(CommandHandler('src', src))
	dispatcher.add_handler(CommandHandler('start', start))
	dispatcher.add_handler(CommandHandler('letter', letter))
	dispatcher.add_handler(MessageHandler(Filters.photo, photo(db_session)))
	dispatcher.add_handler(MessageHandler(Filters.audio, audio(db_session)))

