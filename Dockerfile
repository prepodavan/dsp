FROM python:3.6-slim-stretch

RUN apt-get -y update
RUN apt-get install -y --fix-missing \
    build-essential \
    cmake \
    gfortran \
    git \
    wget \
    curl \
    graphicsmagick \
    libgraphicsmagick1-dev \
    libatlas-dev \
    libavcodec-dev \
    libavformat-dev \
    libgtk2.0-dev \
    libjpeg-dev \
    liblapack-dev \
    libswscale-dev \
    pkg-config \
    python3-dev \
    python3-numpy \
    software-properties-common \
    zip \
    && apt-get clean && rm -rf /tmp/* /var/tmp/*

RUN cd ~ && \
    mkdir -p dlib && \
    git clone -b 'v19.9' --single-branch https://github.com/davisking/dlib.git dlib/ && \
    cd  dlib/ && \
    python3 setup.py install --yes USE_AVX_INSTRUCTIONS

RUN cd ~ && \
	mkdir -p face_recognition && \
	git clone -b 'v1.2.2' --single-branch https://github.com/ageitgey/face_recognition.git face_recognition/ && \
	cd face_recognition/ && \
	pip3 install -r requirements.txt && \
	python3 setup.py install

RUN apt-get install libpq-dev python-dev python3-dev ffmpeg -y


COPY . /code
WORKDIR /code
RUN python3 -m pip install -r requirements.txt
ENTRYPOINT ["python3", "bot.py"]
