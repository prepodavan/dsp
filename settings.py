import os
from pathlib import Path


config = {
	'messages': {
		'repo':'https://bitbucket.org/prepodavan/dsp/src',
		'letter': Path('./letter.txt').open().read(),
		'greetings': Path('./greetings.txt').open().read(),
	},
	'postgres': {
		'URL': os.environ['DATABASE_URL'],
	},
	'bot': {
		'TOKEN': os.environ['TGTOKEN'],
		'PORT':int(os.environ.get('PORT', 8443)),
		'HOST':'0.0.0.0',
		'WEBHOOK': f'https://{os.environ["HEROKU_APP_NAME"]}.herokuapp.com/{os.environ["TGTOKEN"]}',
	}
}