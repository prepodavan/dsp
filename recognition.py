import io
import face_recognition
import numpy as np

def count_faces(image: bytearray):
	converted_image = face_recognition.load_image_file(io.BytesIO(image))
	faces = face_recognition.face_locations(converted_image, number_of_times_to_upsample=0, model="cnn")
	return len(faces)

